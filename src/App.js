import React from "react";
import "./App.css";
import List from "./Components/List";
import NewList from "./Components/NewList";

function App() {
  return (
    <div className="App">
      <h1>Movie List</h1>
      <List />
      <NewList />
    </div>
  );
}

export default App;
