import ArrayReducer from "./Array";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  array: ArrayReducer
});

export default rootReducer;
