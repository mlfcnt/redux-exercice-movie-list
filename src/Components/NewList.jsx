import React from "react";
import { useSelector } from "react-redux";

const NewList = () => {
  const listOfMovies = useSelector(state => state.array);

  return (
    <div>
      <ul>
        {listOfMovies.map(movie => (
          <li>{movie.length}</li>
        ))}
      </ul>
    </div>
  );
};

export default NewList;
