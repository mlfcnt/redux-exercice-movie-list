import React from "react";
import { useSelector } from "react-redux";

const List = () => {
  const listOfMovies = useSelector(state => state.array);

  return (
    <div>
      <ul>
        {listOfMovies.map(movie => (
          <li>{movie}</li>
        ))}
      </ul>
    </div>
  );
};

export default List;
